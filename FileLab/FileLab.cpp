/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 8 Lab 1 - File
*/



#include <iostream>
#include "bookcase.h"
#include <string>
#include <algorithm> 
#include <functional>
#include <cctype> 

using namespace std;

bool isvalidCode(string); //Function prototype (Declaration)
int whichCode(string); //Function prototype (Declaration)

bool isvalidYN(string); //Function prototype (Declaration)
bool yesNo(string); //Function prototype (Declaration)

bool isvalidInt(string); //Function prototype (Declaration)

int main()
{
	Bookcase shelf;
	string checkInput; // User input to get passed to checker function
	bool needInput;
	

	cout << "Chapter 08 Lab: File Lab." << endl << endl;
	cout << "Enter the book list to import." << endl;

	needInput = 1;
	bool needFile = 1;
	string filename;
	while (needInput == 1)
	{
		while (needFile == 1) //enter the file input loop
		{
			//getline(cin, filename); // User enters filename
			filename = "MyBookList.txt"; //TEMP: Skip past filename entry for faster testing
			cout << filename << endl; //TEMP: Skip past filename entry for faster testing
			shelf.fileIn(filename); //Could rework fileIn and this section so as to print an error message, loop into asking again or quitting, but uunneeded. 
			needFile = 0; //Exit the file-loop, which doesn't actually loop because I didn't have it check if the file opens and reprompt for a new filename
		}

		int code;
		bool needCode = 1;

		while (needCode == 1) //enter the action loop
		{
			cout << endl;
			cout << "Do you want to:" << endl;
			cout << "[List] out all books?" << endl;
			cout << "[View] a specific book?" << endl; //enter submenu asking which book to view
			cout << "[Add] a new book?" << endl; 
			cout << "[Delete] a book?" << endl; //enter submenu asking which book to delete
			cout << "[Update] a book?" << endl; //enter submenu asking which book to modify, then which field to modify
			cout << "[Save] your changes?" << endl; //also accept [s], confirm y/n
			cout << "Or [Quit]?" << endl; //also accept [q], confirm y/n
			
			getline(cin, checkInput); // User enters code
			if (!isvalidCode(checkInput)) // Checks if user input is a valid code
			{
				cout << "Please enter only actions in [Brackets]." << endl; //Add joke for user entering [brackets] if there's time
			}
			else
			{
				code = whichCode(checkInput); //Turns the confirmed valid code into an int for the switch
				needCode = 0; // Exit the action loop
			}
		}

		//BIG SWITCH GOES HERE

		switch (code) {

			/*
			Valid codes are
			1 [List]
			2 [View]
			3 [Add]
			4 [Delete]
			5 [Update]
			6 [Save]
			7 [Quit]
			8 [Brackets] //Joke

			//Visual Studio 2017 is fighting me on indentation of every line in this switch, pardon the mess.
			*/
		case 1: 
			{
			//List
			cout << endl;
			shelf.list();
				break;
			}
		case 2:
			{
			//View

			bool needBook = 1, exit = 0;
			int booknumber;
			while (needBook == 1) //enter the Book-choosing loop
			{
				cout << "Which book would you like to view? Enter its number. Enter [0] to cancel." << endl;
				
				getline(cin, checkInput); // User enters book number
				if (!isvalidInt(checkInput)) // Checks if user input is a valid integer
				{
					cout << "Please enter the book's number." << endl;
				}
				else
				{
					booknumber = atoi(checkInput.c_str()); // converts string to int

					if (booknumber == 0)
					{
						cout << "Viewing cancelled." << endl;
						exit = 1;
						needBook = 0;
					}
					else if (booknumber > shelf.length())
					{
						cout << "There is no book #";
						if (booknumber < 10) //Formatting stuff
						{
							cout << setfill('0') << setw(2) << booknumber;
						}
						else
						{
							cout << booknumber;
						}
						cout << "." << endl;
					}
					else
					{
						needBook = 0; //Exit the bookchoosing loop with a book chosen
					}
				}
			}
			if (exit == 1)
			{
				break;
			}
			else
			{
				shelf.view(booknumber);
				break;
			}
			}
		case 3:
			{
			//Add
			/* Field ordering:
			1 Book Title
			2 Edition
			3 Author(s)
			4 Publisher
			5 Copyright Date
			6 ISBN-13
			7 Book Type
			8 Subject
			*/

			const int NUMBEROFFIELDS= 8;
			string fields[NUMBEROFFIELDS] = { "Title", "Edition", "Author", "Publisher", "Copyright Date", "ISBN-13", "Book Type", "Subject" };
			string info[NUMBEROFFIELDS];

			cout << "Adding a book through user input." << endl;
			bool needInfo = 1;
			for (int x = 0; needInfo == 1; x++)
			{
				cout << "Please insert the " << fields[x] << "." << endl; //Asks the user to enter Book information, one field at a time.
				getline(cin, checkInput); // User enters 
				info[x] = checkInput;
				if (x + 1 == NUMBEROFFIELDS) //Hit the end of fields
				{
					needInfo = 0;
				}
			}

			shelf.add(info);
			cout << "Book added." << endl;
				break;
			}
		case 4:
			{
			//Delete

			bool needBook = 1, exit = 0;
			int booknumber;
			while (needBook == 1) //enter the Book-choosing loop
			{
				cout << "Which book would you like to delete? Enter its number. Enter [0] to cancel." << endl;

				getline(cin, checkInput); // User enters book number
				if (!isvalidInt(checkInput)) // Checks if user input is a valid integer
				{
					cout << "Please enter the book's number." << endl;
				}
				else
				{
					booknumber = atoi(checkInput.c_str()); // converts string to int

					if (booknumber == 0)
					{
						cout << "Deletion cancelled." << endl;
						exit = 1;
						needBook = 0;
					}
					else if (booknumber > shelf.length())
					{
						cout << "There is no book #";
						if (booknumber < 10)
						{
							cout << setfill('0') << setw(2) << booknumber;
						}
						else
						{
							cout << booknumber;
						}
						cout << "." << endl;
					}
					else
					{
						needBook = 0; //Exit the bookchoosing loop with a book chosen
					}
				}
			}
			if (exit == 1)
			{
				break;
			}
			else
			{
				shelf.kill(booknumber);
				cout << "Book deleted." << endl;
				break;
			}
				break;
			}
		case 5:
			{
			//Update

			/* Field ordering:
			1 Book Title
			2 Edition
			3 Author(s)
			4 Publisher
			5 Copyright Date
			6 ISBN-13
			7 Book Type
			8 Subject
			*/

			const int NUMBEROFFIELDS = 8;
			string fields[NUMBEROFFIELDS] = { "Title", "Edition", "Author", "Publisher", "Copyright Date", "ISBN-13", "Book Type", "Subject" };
			string info[NUMBEROFFIELDS];
			bool needBook = 1, exit = 0;
			int booknumber;

			while (needBook == 1) //enter the Book-choosing loop
			{
				cout << "Which book would you like to update? Enter its number. Enter [0] to cancel." << endl;

				getline(cin, checkInput); // User enters book number
				if (!isvalidInt(checkInput)) // Checks if user input is a valid integer
				{
					cout << "Please enter the book's number." << endl;
				}
				else
				{
					booknumber = atoi(checkInput.c_str()); // converts string to int

					if (booknumber == 0) 
					{
						cout << "Update cancelled." << endl;
						exit = 1;
						needBook = 0;
					}
					else if (booknumber > shelf.length())
					{
						cout << "There is no book #";
						if (booknumber < 10)
						{
							cout << setfill('0') << setw(2) << booknumber;
						}
						else
						{
							cout << booknumber;
						}
						cout << "." << endl;
					}
					else
					{
						needBook = 0; //Exit the bookchoosing loop with a book chosen
					}
				}
			}
		
			if (exit == 1)
			{
				break;
			}
			else
			{
				shelf.getInfo(booknumber, info);
				int field;
				bool needField = 1, updating = 1;
				while (updating == 1)
				{
					while (needField == 1)
					{
						cout << "Which field would you like to modify? Enter [0] to end." << endl;
						for (int x = 1; x < NUMBEROFFIELDS; x++) //Prints the list of avaliable fields that can be modified, as well as the current contents of said fields
						{
							cout << "[" << x << "]: " << fields[x - 1] << ": " << info[x - 1] << endl;
						}

						getline(cin, checkInput); // User enters field number
						if (!isvalidInt(checkInput)) // Checks if user input is a valid integer
						{
							cout << "Please enter the number of the field you wish to modify." << endl;
						}
						else
						{
							field = atoi(checkInput.c_str()); // converts string to int

							if (field == 0)
							{
								cout << "Update ended." << endl;
								exit = 1;
								needField = 0;
							}
							else if (field > NUMBEROFFIELDS)
							{
								cout << "There is no field #";
								if (field < 10)
								{
									cout << setfill('0') << setw(2) << field;
								}
								else
								{
									cout << field;
								}
								cout << "." << endl;
							}
							else
							{
								field = field - 1; //Converts from a list starting a 1 to one starting at 0
								needField = 0; //Exit the fieldchoosing loop with a field chosen
							}
						}
					}
					if (exit == 1)
					{
						updating = 0;
					}
					else
					{
						cout << "Please enter the new information for the " << fields[field] << "." << endl;
						getline(cin, checkInput); // User enters new information
						info[field] = checkInput;
						cout << fields[field] << " Updated." << endl;
						needField = 1; //Need the next field to modify/exit code
					}
				}

				//Modify the actual book here
				shelf.updateBook(booknumber, info);
				cout << "Book updated" << endl;
			}

				break;
			}
		case 6:
			{
			//Save
			bool needYN = 1;
			bool yn = 0;
			while (needYN == 1)
			{
				cout << "Are you sure you want to save?" << endl << "[Y]es/[N]o" << endl;

				getline(cin, checkInput); // User enters Yes/No
				if (!isvalidYN(checkInput)) // Checks if user input is a valid Yes/No
				{
					cout << "Please enter only Yes or No." << endl;
				}
				else
				{
					yn = yesNo(checkInput);
					needYN = 0; // Exit the YN checker
				}

			}

			if (yn == 1)
			{
				cout << "Save confirmed." << endl;
				shelf.save(filename);
				break;
			}
			else
			{
				cout << "Save cancelled." << endl;
				break;

			}

				break;
			}
		case 7:
			{
			//Quit
			bool needYN = 1;
			bool yn = 0;
			while (needYN == 1)
			{
				cout << "Are you sure you want to quit?" << endl << "[Y]es/[N]o" << endl;

				getline(cin, checkInput); // User enters Yes/No
				if (!isvalidYN(checkInput)) // Checks if user input is a valid Yes/No
				{
					cout << "Please enter only Yes or No." << endl; 
				}
				else
				{
					yn = yesNo(checkInput);
					needYN = 0; // Exit the YN checker
				}

			}

			if (yn == 1)
			{
				cout << "Quit confirmed." << endl;
				needInput = 0;
				break;
			}
			else
			{
				cout << "Quit cancelled." << endl;
				break;

			}

			}
		case 8:
			{
			//Bracket, Joke option.
				cout << "Please don't joke around." << endl; 
				break;
			}

		}
	}

	system("pause");
	return 0;
}


bool isvalidCode(string str) //checks a string to see if it's a valid code
{
	/*	
	case insensitive
	Valid codes are
	1 [List]
	2 [View]
	3 [Add]
	4 [Delete]
	5 [Update]
	6 [Save]
	7 [Quit]
	8 [Brackets] //Joke
	*/
	const int NUMBEROFCODES = 8;
	const int NUMBEROFALTCODES = 7;
	string codes[NUMBEROFCODES] = {"LIST", "VIEW", "ADD", "DELETE", "UPDATE", "SAVE", "QUIT", "BRACKETS"};
	string altCodes[NUMBEROFALTCODES] = { "L", "V", "A", "D", "U", "S", "Q"}; //Accept just the first letter for these codes
	bool valid = true; //assume a valid
						   
	if (int(str.length()) == 0) //Check for an empty string
	{
		valid = false;
		return valid;
	}

	//Check if there are more than eight characters in the string, the longest of the acceptable codes
	if (int(str.length()) > 8)
	{
		valid = false;
		return valid;
	}

	//Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper)); 

	bool checking = 1;

	if (str.length() == 1) //If the string might be a one-character alt-code
	{
		for (int x = 0; checking == 1; x++) //Check for each of the alt-codes
		{
			if (str.compare(altCodes[x]) == 0) //if the string compares equal to an alt-code
			{
				valid = true;
				return valid;
			}

			if (x+1 == NUMBEROFALTCODES) //Hit the end of the alt code list, didn't find a hit
			{
				checking = 0;
			}
		}

		//didn't match the codes, string too short to bother checking the full codes array
		valid = false;
		return valid;
	}

	for (int x = 0; checking == 1; x++) //Check for each code
	{
		if (str.compare(codes[x]) == 0) //if the string compares equal to a code
		{
			valid = true;
			return valid;
		}

		if (x+1 == NUMBEROFCODES) //Hit the end of the code list, didn't find a hit
		{
			checking = 0;
		}
	}

	//didn't match the codes
	valid = false;
	return valid;
}

bool isvalidYN(string str) //checks a string to see if it's Y/N (or yes/no)
{
	/*
	case insensitive
	Valid codes are
	1 Yes
	2 No
	3 Y
	4 N
	*/

	const int NUMBEROFCODES = 4;
	string codes[NUMBEROFCODES] = { "YES", "NO", "Y", "N" };

	bool valid = true; //assume a valid

	if (int(str.length()) == 0) //Check for an empty string
	{
		valid = false;
		return valid;
	}

	//Check if there are more than three characters in the string, the longest of the acceptable inputs
	if (int(str.length()) > 3)
	{
		valid = false;
		return valid;
	}

	//Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper));

	bool checking = 1;
	for (int x = 0; checking == 1; x++) //Check for each code
	{
		if (str.compare(codes[x]) == 0) //if the string compares equal to an alt-code
		{
			valid = true;
			return valid;
		}

		if (x+1 == NUMBEROFCODES) //Hit the end of the code list, didn't find a hit
		{
			checking = 0;
		}
	}
	//Didn't match the codes
	valid = false;
	return valid;
}


int whichCode(string str) //converts a string containing a valid action code into an int for use in the switch
{
	/*
	Valid codes are
	1 [List]
	2 [View]
	3 [Add]
	4 [Delete]
	5 [Update]
	6 [Save]
	7 [Quit]
	8 [Brackets] //Joke
	*/
	const int NUMBEROFCODES = 8;
	const int NUMBEROFALTCODES = 7;

	int code = 0;
	string codes[NUMBEROFCODES] = { "LIST", "VIEW", "ADD", "DELETE", "UPDATE", "SAVE", "QUIT", "BRACKETS" };
	string altCodes[NUMBEROFALTCODES] = { "L", "V", "A", "D", "U", "S", "Q" }; //Accept just the first letter for codes other than the joke one


	//Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper));

	bool checking = 1;
	if (str.length() == 1) //If the string's a one-character alt-code
	{
		for (int x = 0; checking == 1; x++) //Check for each of the alt-codes
		{
			if (str.compare(altCodes[x]) == 0) //if the string compares equal to an alt-code
			{
				code = x + 1; //Found the code, set code to indicate which it is. x+1 because the switch starts from 1, rather than 0
				return code;
			}

			if (x+1 == NUMBEROFALTCODES) //Hit the end of the alt code list, didn't find a hit
			{
				checking = 0;
				//Shouldn't get here, as this function should only take validated codes
			}
		}
		checking = 1;
	}

	for (int x = 0; checking == 1; x++) //Check for each code
	{
		if (str.compare(codes[x]) == 0) //if the string compares equal to a code
		{
			code = x + 1; //Found the code, set code to indicate which it is. x+1 because the switch starts from 1, rather than 0
			return code;
		}

		if (x+1 == NUMBEROFCODES) //Hit the end of the code list, didn't find a hit
		{
			checking = 0;
			//Shouldn't get here, as this function should only take validated codes
		}
	}

	//didn't match the codes, which is an error. Return a quit code
	return 7;
}

bool yesNo(string str) //Takes a validated Yes/No string and determines if Yes or No was entered. Returns 1 for Yes, 0 for no.
{
	/*
	case insensitive
	Valid codes are
	1 Yes
	2 No
	3 Y
	4 N
	*/

	const int NUMBEROFCODES = 4;
	string codes[NUMBEROFCODES] = { "YES", "NO", "Y", "N" };

	//Convert string to uppercase
	std::transform(str.begin(), str.end(), str.begin(), ptr_fun<int, int>(toupper));

	bool yn = 0; //Assume "No"

	if (str.compare(codes[0]) == 0 || str.compare(codes[2]) == 0) //String is equal to codes 1 and 3, that is to say the yeses
	{
		yn = 1;
		return yn;
	}
	else
	{
		return yn;
	}
}

bool isvalidInt(string str)
{
	int start = 0;// Location to start checking string
	int i; // Location in string checker is at
	bool valid = true; //assume a valid
	bool sign = false; //assume no sign


					   //Check for an empty string
	if (str.empty())
	{
		valid = false;
		return valid;
	}

	//Check for a leading sign
	if (str.at(0) == '-' || str.at(0) == '+')
	{
		sign = true;
		if (str.at(0) == '-') //No negatives allowed
		{
			valid = false;
			return valid;
		}
		start = 1; //Start checking for digits after the sign
	}

	//Check that there is at least one character after the sign
	if (sign && int(str.length()) == 1) valid = false;

	// Now check the string, which you know has at least one non-sign char
	i = start;
	while (valid && i < int(str.length()))
	{
		if (!isdigit(str.at(i))) valid = false; //Found a non digit
		i++; //Move to next character
	}
	return valid;
}