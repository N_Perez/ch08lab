/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 8 Lab 1 - File
book case
*/


#ifndef BOOKCASE_H
#define BOOKCASE_H

#include "book.h"
#include "ldeque.h"
#include <iomanip>
#include <stdio.h>

using namespace std;


class Bookcase : public LDeck<Book>
{
	Book* helpGetBook(int booknumber) //returns a pointer to a specific book, as specified by the book number
	{
		booknumber = booknumber - 1; //Adjusts the booknumber from a list starting at 1 to one starting at 0

		if (booknumber < 0 || booknumber > length()) //Checks if the booknumber given is 0, negative, or out of bounds
		{
			cout << "There is no book #";
			if (booknumber < 10)
			{
				cout << setfill('0') << setw(2) << booknumber;
			}
			else
			{
				cout << booknumber;
			}
			cout << "." << endl;
			abort();
		}

		Book temp;
		Book* outBook;
		Link<Book>* tempLink;
		LDeck<Book> tempdeck; // Temp deck for books we pull while getting to the book we want

		for (int x = 0; x < booknumber; x++) //Pulls the books in the way into a holding deck
		{
			temp = dequeue(); //Pulls a book out
			tempdeck.enqueue(temp); //Puts it in a holding deck
		}

		tempLink = frontLink(); //Gets a pointer to the front link
		outBook = &(tempLink->element); //Gets a pointer to the book

		while (tempdeck.length() > 0) //stuffs the books from the holding deck back into the bookcase
		{
			temp = tempdeck.dedeck();
			endeck(temp);
		}

		return outBook;

		//Could rework to avoid the tempdecks and dequeuing via setting outbook equal to the front pointer and then looping outbook=outbook->next until it hits booknumber but this works
	}

public:

	void fileIn(string filename) //Reads a set of books from a given filename, loads the bookcase with it
	{
		string line;
		ifstream myfile(filename); //Open the word list
		if (myfile.is_open())
		{
			cout << "Loading books from \"" << filename << "\"." << endl;
			string info[8]; //Array to hold the book information before it is inserted into the book object
			//cout << "Inserting: " << endl;

			int x = 0;
			while (getline(myfile, line)) // Grabs each line from the text file, stores them in the info array
			{
				if (x == 8) //Hit the 9th line, the blank one
				{

					//Book temp(info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7]); //Filling a temp book via the constructor

					Book temp; //Temporary book to hold the information before it is inserted into the bookcase
					temp.setAll(info);//Fills the temp book with out array of information					
					enqueue(temp); //Stores the book in the bookcase
					x = 0; //Sets x back to 0 to start on the next book's information
				}
				else
				{
					info[x] = line; //Puts a line of information into the appropriate slot in the array
					x++;
				}

			}
			cout << "Finished loading." << endl;
			myfile.close(); // Closes the file
		}

		else
		{
			cout << "Unable to open file.";
			abort(); //Can't open the file, print an error and kill program
		}
	}

	void list() //Lists the title and author of every book in the bookcase
	{
		Book* temp; //Pointer to the book we're printing
		int x = 1;
		for (int x = 1; x-1 < length(); x++)
		{
			if (length() == 1) //Last book
			{
				temp = helpGetBook(x); //Gets a pointer to the [x]th book
				cout << "Book #";
				if (x < 10) //Formatting stuff
				{
					cout << setfill('0') << setw(2) << x;
				}
				else
				{
					cout << x;
				}
				cout << ":" << endl;

				temp->printShort();
				cout << "End of list." << endl;
			}
			else
			{
				temp = helpGetBook(x); //Gets a pointer to the [x]th book
				cout << "Book #";
				if (x < 10) //Formatting stuff
				{
					cout << setfill('0') << setw(2) << x;
				}
				else
				{
					cout << x;
				}
				cout << ":" << endl;
				temp->printShort();
			}
		}

	}

	void view(int booknumber) //Prints all the details of a given book, indicated by its position in the deck
	{
		
		Book* temp;
		temp = helpGetBook(booknumber);

		temp->printAll(); //Prints its info
	}

	void add(string info[]) //Adds a book to the bookcase, info generated via a string array
	{
		/* 
		Book Title
		Edition
		Author(s)
		Publisher
		Copyright Date
		ISBN-13
		Book Type
		Subject		
		*/
		Book temp; // Temp book to hold our information
		temp.setAll(info); //Fills in the temp book
		enqueue(temp); //Adds the filled in temp book to the bookcase
	}

	void kill(int booknumber) //Removes a given book, indicated by its position in the deck
	{
		booknumber = booknumber - 1; //Adjusts the booknumber from a list starting at 1 to one starting at 0

		if (booknumber < 0 || booknumber > length()) //Checks if the booknumber given is 0, negative, or out of bounds
		{
			cout << "There is no book #";
			if (booknumber < 10)
			{
				cout << setfill('0') << setw(2) << booknumber;
			}
			else
			{
				cout << booknumber;
			}
			cout << "." << endl;
			return;
		}

		Book temp;
		LDeck<Book> tempdeck; // Temp deck for books we pull while getting to the book we want

		for (int x = 0; x < booknumber; x++) //Pulls the books in the way into a holding deck
		{
			temp = dequeue(); //Pulls a book out
			tempdeck.enqueue(temp); //Puts it in a holding deck
		}

		temp = dequeue(); //Removes the book we want 
		

		while (tempdeck.length() > 0) //stuffs the books from the holding deck back into the bookcase
		{
			temp = tempdeck.dedeck();
			endeck(temp);
		}
	}
	
	void getInfo(int booknumber, string info[]) //Gets all of the information of a specific book and returns it in a given array
	{
		Book* temp; //Pointer to the book we're scraping info from
		temp = helpGetBook(booknumber); //Sets the pointer to the right book
		temp->getAll(info); //Fills the info array
	}


	void updateBook(int booknumber, string info[]) //Updates all the information of a specified book with the information from a given array
	{

		Book* temp; //Pointer to the book we're updating
		temp = helpGetBook(booknumber); //Sets the pointer to the right book
		temp->setAll(info); //Fills the info array
	}

	void save(string filename) //Saves the current bookcase to the specified filename
	{
		ifstream myfile(filename); //Open the word list
		if (myfile.is_open())
		{
			remove((filename.c_str()));//Delete the file
			myfile.close(); // Closes the file. Do I need to do this if I'm deleting it?
		}
		else
		{
			cout << "Can't delete " << filename << endl;
			//Can't open the file, don't think this should cause a problem
		}

		cout << "Saving books to \"" << (filename.c_str()) << "\"." << endl;
		Book* temp; //Pointer to each book
		string info[8]; //Array to hold the book information before it is saved to the file

		ofstream outfile((filename.c_str())); //Creates a text file we're going to save everything to 

		if (outfile.is_open())
		{
			for (int x = 1; x - 1 < length(); x++) //Takes each book in the bookcase and saves it
			{

				temp = helpGetBook(x); //Sets the pointer to the right book
				temp->getAll(info); //Fills the info array

				for (int y = 0; y < 8; y++) //Operates on each field in the info array
				{
					if (y == 7) //Hit the last field
					{
						outfile << info[y] << endl << endl;
					}
					else
					{
						outfile << info[y] << endl;
					}
				}
			}
			outfile.close(); //Closes the finished file
		}

		else cout << "Unable to open file" << endl; 

		cout << "Finished saving." << endl;
	}
};
#endif