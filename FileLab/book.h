/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 8 Lab 1 - File
book class
*/

#ifndef BOOK_H
#define BOOK_H

#include <string> 

using namespace std;

class Book
{
	string title;
	string edition;
	string author;
	string publisher;
	string copyright;
	string isbn;
	string type;
	string subject;

public:

	// Constructors
	Book(string titleIn, string editionIn, string authorIn, string publisherIn, string copyrightIn, string isbnIn, string typeIn, string subjectIn) //Constructor, with initial data supplied
	{
		title = titleIn;
		edition = editionIn;
		author = authorIn;
		publisher = publisherIn;
		copyright = copyrightIn;
		isbn = isbnIn;
		type = typeIn;
		subject = subjectIn;
	}
	Book(string info[]) //Constructor with values, accepts an 8 slot string array
	{
		title = info[0];
		edition = info[1];
		author = info[2];
		publisher = info[3];
		copyright = info[4];
		isbn = info[5];
		type = info[6];
		subject = info[7];
	}
	Book() //Constructor, without initial data
	{

		title = "Unknown";
		edition = "Unknown";
		author = "Unknown";
		publisher = "Unknown";
		copyright = "Unknown";
		isbn = "Unknown";
		type = "Unknown";
		subject = "Unknown";
	}

	void getAll(string info[]) //Fills the given array with the book's information
	{
		info[0] = title;
		info[1] = edition;
		info[2] = author;
		info[3] = publisher;
		info[4] = copyright;
		info[5] = isbn;
		info[6] = type;
		info[7] = subject;
	}
	void setAll(string info[]) //Overrites the book's information with that from an array
	{
		title = info[0];
		edition = info[1];
		author = info[2];
		publisher = info[3];
		copyright = info[4];
		isbn = info[5];
		type = info[6];
		subject = info[7];
	}

	string getTitle()
	{
		return title;
	}
	void setTitle(string titleIn)
	{
		title = titleIn;
	}

	string getEdition()
	{
		return edition;
	}
	void setEdition(string editionIn)
	{
		edition = editionIn;
	}

	string getAuthor()
	{
		return author;
	}
	void setAuthor(string authorIn)
	{
		author = authorIn;
	}

	string getPublisher()
	{
		return publisher;
	}
	void setPublisher(string publisherIn)
	{
		publisher = publisherIn;
	}

	string getCopyright()
	{
		return copyright;
	}
	void setCopyright(string copyrightIn)
	{
		copyright = copyrightIn;
	}

	string getISBN()
	{
		return isbn;
	}
	void setISBN(string isbnIn)
	{
		isbn = isbnIn;
	}

	string getType()
	{
		return type;
	}
	void setType(string typeIn)
	{
		type = typeIn;
	}

	string getSubject()
	{
		return subject;
	}
	void setSubject(string subjectIn)
	{
		subject = subjectIn;
	}

	void printShort() //Prints the Title and Author of a book
	{
		cout << "TITLE: " << getTitle() << endl;
		cout << "AUTHOR: " << getAuthor() << endl;
		cout << endl;
	}

	void printAll() //Prints all the details of a book
	{
		cout << "TITLE: " << getTitle() << endl;
		cout << "EDITION: " << getEdition() << endl;
		cout << "AUTHOR: " << getAuthor() << endl;
		cout << "PUBLISHER: " << getPublisher() << endl;
		cout << "COPYRIGHT DATE: " << getCopyright() << endl;
		cout << "ISBN-13: " << getISBN() << endl;
		cout << "BOOK TYPE: " << getType() << endl;
		cout << "SUBJECT: " << getSubject() << endl;
		cout << endl;
	}
};

#endif